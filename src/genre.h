#ifndef GENRE_H
#define GENRE_H

#include <iostream>
#include <string>

class Genre
{
 public:
  Genre();
  ~Genre();
  Genre(unsigned int, std::string);
  unsigned int  get_id();
  void set_id(unsigned int);
  std::string get_type();
  void set_type(std::string);

 private :
  unsigned int id;
  std::string type;
};

#endif

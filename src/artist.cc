#include "artiste.h"

#include <iostream>
#include <string>

Artiste::Artiste() :
  id(0),
  name("")
{}

Artiste::Artiste(unsigned int _id, std::string _name) :
  id(_id),
  name(_name)
{}

Artiste::~Artiste()
{
  std::cout << "Destructeur par défaut" << std::endl;
}

unsigned int Artiste::get_id()
{
  return id;
}

void Artiste::set_id(unsigned int _id)
{
  id = _id;
}

std::string Artiste::get_name()
{
  return name;
}

void Artiste::set_name(std::string _name)
{
  name = _name;
}

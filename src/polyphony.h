#ifndef POLYPHONY_H
#define POLYPHONY_H

#include <iostream>
#include <string>

class Polyphony
{
 public:
  Polyphony();
  ~Polyphony();
  Polyphony(unsigned int, short int);
  unsigned int get_id();
  void  set_id(unsigned int);
  short int get_number();
  void set_number(short int);

 private :
  unsigned int id;
  short int number;
};

#endif

#include "cli.h"

DEFINE_int64(duration, 4000, "duration of the playlist");
DEFINE_string(genre, "", "genre of playlist");
DEFINE_string(type, "", "file extention");
DEFINE_string(subgenre, "", "Subgenre of Playlist");
DEFINE_string(playlist, "", "Name of the playlist");

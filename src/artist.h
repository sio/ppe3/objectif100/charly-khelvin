#ifndef ARTISTE_H
#define ARTISTE_H

#include <iostream>
#include <string>

class Artist
{
 public:
  Artist();
  ~Artist();
  Artist(unsigned int, std::string);
  unsigned int get_id();
  void set_id(unsigned int);
  std::string get_name();
  void set_name(std::string);

 private :
  unsigned int id;
  std::string name;
};

#endif
